import numpy as np
import matplotlib.pyplot as plt

def error_N1():
  N = 20                      # the number of intervals to divide space
  ymin = physProps['ymin']
  ymax = physProps['ymax']
  dy = (ymax-ymin)/N              # the length of spacing

  y = np.linspace(ymin,ymax,N+1)         # the spatial dimension
  
  p = pressure(y,physProps)            # the pressure at the y point 
  uExact = uGeoExact(y,physProps)         # the exact geostrophicwind

  dpdy = gradient_2point_new(p,dy)          # the pressure gradient
  u_2point = geoWind(dpdy,physProps)       # the wind

  error_20 = u_2point-uExact
  return error_20,dy

def error_N2():
  N = 40                      # the number of intervals to divide space
  ymin = physProps['ymin']
  ymax = physProps['ymax']
  dy = (ymax-ymin)/N              # the length of spacing

  y = np.linspace(ymin,ymax,N+1)         # the spatial dimension
  
  p = pressure(y,physProps)            # the pressure at the y point 
  uExact = uGeoExact(y,physProps)         # the exact geostrophicwind

  dpdy = gradient_2point_new(p,dy)          # the pressure gradient
  u_2point = geoWind(dpdy,physProps)       # the wind

  error_40 = u_2point-uExact
  return error_40,dy

n_endpoint_left = (np.log(abs(error_N1()[0][0]))-np.log(abs(error_N2()[0][0])))/(np.log(error_N1()[-1])-np.log(error_N2()[-1]))
n_endpoint_right = (np.log(abs(error_N1()[0][-1]))-np.log(abs(error_N2()[0][-1])))/(np.log(error_N1()[-1])-np.log(error_N2()[-1]))
n_midpoint = (np.log(abs(error_N1()[0][10]))-np.log(abs(error_N2()[0][20])))/(np.log(error_N1()[-1])-np.log(error_N2()[-1]))
print('order of left end point =',n_endpoint_left)
print('order of right end point =',n_endpoint_right)
print('order of mid right end point =',n_midpoint)