import numpy as np
import matplotlib.pyplot as plt

physProps = {'pa':1e5,   # the mean pressure
       'pb':200,   # the magnitude of the pressure variations
       'f':1e-4,   # the Carolios parameter 
       'rho':1.,   # density
       'L':2.4e6,  # length scale of the pressure variations(k)
       'ymin':0.,  # start of the y domain
       'ymax':1e6,} # end of the y domain
def pressure(y,props):
  '''the pressure and given y locations based on dictionary of physical properties,props'''
  pa = props['pa']
  pb = props['pb']
  L = props['L']
  return pa+pb*np.cos(y*np.pi/L)
def uGeoExact(y,props):
  '''the analytic geostrophic wind at given locations, y based on dictionary of physical properties, props'''
  pb = props['pb']
  L = props['L']
  rho = props['rho']
  f = props['f']
  return pb*np.pi/(rho*f*L)*np.sin(y*np.pi/L)
def geoWind(dpdy,props):
  '''the geostrophic wind as a function of pressure gradient based on dictionary of physical properties,props'''
  rho = props['rho']
  f = props['f']
  return -dpdy/(rho*f)
  
def gradient_2point_new(f,dx):
  '''the gradient of one dimensional array f assuming points are a distance dx/
  apart using 2-point differences. Returns an array the same size as f'''

  dfdx = np.zeros_like(f)             # initialised the array for the gradient to be the same size as f

  dfdx[0] = (-f[2]+4*f[1]-3*f[0])/(2*dx)          # two point differences at the end points 
  dfdx[-1] = (f[-3]-4*f[-2]+3*f[-1])/(2*dx)

  for i in range(1,len(f)-1):           # centred differences at the mid points
    dfdx[i] = (f[i+1]-f[i-1])/(2*dx)

  return dfdx
  
def geostrophicWind():
  '''calculate the geostrophicwind with analytically and numerically and plot'''

  N=10                      # the number of intervals to divide space
  ymin = physProps['ymin']
  ymax = physProps['ymax']
  dy = (ymax-ymin)/N              # the length of spacing

  y = np.linspace(ymin,ymax,N+1)         # the spatial dimension
  
  p = pressure(y,physProps)            # the pressure at the y point 
  uExact = uGeoExact(y,physProps)         # the exact geostrophicwind

  dpdy = gradient_2point_new(p,dy)          # the pressure gradient
  u_2point = geoWind(dpdy,physProps)       # the wind

#graph to compare the numerical and analytic solutions

  font = {'size':14}               # plot using large fonts
  plt.rc('font',**font)


  plt.plot(y/1000,uExact,'k-',label='Exact')
  plt.plot(y/1000,u_2point,'*k--',label='Two-point differences',\
           ms=12,markeredgewidth=1.5,markerfacecolor='none')
  
  plt.legend(loc='best')
  plt.xlabel('y(km)')
  plt.ylabel('u(m/s)')

  plt.tight_layout()
  plt.savefig('geoWindCent.pdf')
  plt.show()


# plot the errors

  plt.plot(y/1000,u_2point-uExact,'*k--',label='Two-point differences',\
           ms=12,markeredgewidth=1.5,markerfacecolor='none')
  plt.ylim(-0.05,0.20)
  
  plt.legend(loc='best')
  plt.axhline(linestyle='-',color='k')
  plt.xlabel('y(km)')
  plt.ylabel('u(m/s)')

  plt.tight_layout()
  plt.savefig('geoWindErrorsCent.pdf')
  plt.show()

geostrophicWind()